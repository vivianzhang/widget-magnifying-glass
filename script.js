window.onload = function() {

Photo = Backbone.Model.extend();

Photos = Backbone.Collection.extend({
  model: Photo,
  url: 'https://distillery.pixlee.com/api/v2/albums/2652/photos?api_key=HJIdsxhO3DjU1VDG8RX&per_page=16',
  parse: function(response) {
    return response.data;
  },
  sync: function(method, model, options){
    var that = this;
    var params = _.extend({
      type: 'GET',
      dataType: 'json',
      url: that.url,
      processData: false
    }, options);

    return $.ajax(params);
  }
});

PhotosView = Backbone.View.extend({
  initialize: function() {
    _.bindAll(this, 'render');
    this.collection = new Photos;

    var that = this;
    this.collection.fetch({
      success: function(data) {
        that.render();
      }
    });
  },

  render: function() {
    $.each(this.collection.models, function(i, e){
      $('.big_wrapper ul').append('<li class="photoLarge" style="background-image:url(\'' + e.attributes.medium_url + '\')"></li>');
      $('#leftWrapper ul').append('<li style="background-image:url(\'' + e.attributes.medium_url + '\')"></li>');
      $('#rightWrapper ul').append('<li style="background-image:url(\'' + e.attributes.medium_url + '\')"></li>');
    });
    animate();
  }
});

var app = new PhotosView({
  el: $('body')
});

function animate(){
    // social media icons
    $('.big_wrapper > ul > li:nth-child(1)').append('<div class=\"attribution_container\"><div class=\"social_media_icon_container instag\"></div><a href=\"\"><div class=\"user_handle_container\">@mirajmohsin</div></a></div><ul class=\"overlay\"><li style=\"background-image: url(\'http://s7d9.scene7.com/is/image/CharlotteRusse/301672419_496\')\"><a href=\"http://www.pixlee.com/\"></a></li><li style=\"background-image: url(\'http://s7d9.scene7.com/is/image/CharlotteRusse/301661236_600\')\"><a href=\"http://www.pixlee.com/\"></a></li><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301704342_653);\"><a href=\"http://www.pixlee.com/\"></a></li><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301724946_001);\"><a href=\"http://www.pixlee.com/\"></a></li></ul>');
    $('.big_wrapper > ul > li:nth-child(2)').append('<div class=\"attribution_container\"><div class=\"social_media_icon_container instag\"></div><a href=\"\"><div class=\"user_handle_container\">@taiwei426</div></a></div><ul class=\"overlay\"><li style=\"background-image: url(\'http://s7d9.scene7.com/is/image/CharlotteRusse/301661236_600\')\"></li><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301704342_653);\"></li><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301724946_001);\"></li></ul>');
    $('.big_wrapper > ul > li:nth-child(3)').append('<div class=\"attribution_container\"><div class=\"social_media_icon_container instag\"></div><a href=\"\"><div class=\"user_handle_container\">@taiwei426</div></a></div><ul class=\"overlay\"><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301704342_653);\"></li><li style=\"background-image:url(http://s7d9.scene7.com/is/image/CharlotteRusse/301724946_001);\"></li></ul>');

  //Setting
  var pictureRatio =.5;
  var responseTime = 200;
  var animationTime = 150;

  // left frame
  var scrollTogetherABC = $("#leftWrapper");
  // center magnify frame
  var scrollTogetherBAC = $(".big_wrapper");
  // right frame
  var scrollTogetherCAB = $("#rightWrapper");

  //grab the width
  var photoLargeWidth = $('.photoLarge').width();
  var frameWidth = $(window).width();
  var selector = '.big_wrapper_list li';

  //initialize first frame to active
  $("#big_wrapper_list li:nth-child(1)").addClass("active");
  //set listener and bounce animation
  $(".big_wrapper").scroll(function(){
    var oldPos = $(".big_wrapper").scrollLeft();
    //console.log("oldest:"+oldPos)
    // delay script to allow user to scroll min later
    setTimeout(function() { 
      var currentPos = $(".big_wrapper").scrollLeft();
      //console.log("current:"+currentPos);
      //console.log("old:"+oldPos)
      //console.log("oldPos = " + oldPos);
      if (oldPos === currentPos) {
        var currentFrame = Math.round(oldPos/photoLargeWidth);
        var newPos = currentFrame*photoLargeWidth;
        currentFrame += 1;
      //console.log("new = " + newpos);
      $(".big_wrapper").animate({scrollLeft:(newPos)},animationTime);
      $("#big_wrapper_list li").parent().find('li').removeClass("active");
      $("#big_wrapper_list li:nth-child("+currentFrame+")").addClass("active");
    }
  }, responseTime);
  });


  //setting size of smaller picture
  //left scroller
  $( "#small_wrapper_list li" ).each(function() {
    this.style.width = photoLargeWidth*pictureRatio;
    this.style.height = photoLargeWidth*pictureRatio;
  });
  /*document.getElementById("leftContainer").style.height = photoLargeWidth*pictureRatio;*/
  document.getElementById("leftWrapper").style.height = photoLargeWidth*pictureRatio;
  //right scroller
  $( "#small_wrapper_2_list li" ).each(function() {
    this.style.width = photoLargeWidth*pictureRatio;
    this.style.height = photoLargeWidth*pictureRatio;
  });
  /*  document.getElementById("rightContainer").style.height = photoLargeWidth*pictureRatio;*/
  document.getElementById("rightWrapper").style.height = photoLargeWidth*pictureRatio;


  //shift sync the frames
  var numOfImage = $("#small_wrapper_list li").size();
  var MarginToShift = ((frameWidth / 2) - (photoLargeWidth / 2));
  var MarginToShiftRight = photoLargeWidth/2 - photoLargeWidth*pictureRatio;
  document.getElementById("small_wrapper_list").style.width = ((numOfImage-1)*photoLargeWidth*pictureRatio + MarginToShift);
  document.getElementById("small_wrapper_list").style.marginLeft = MarginToShift;
  document.getElementById("small_wrapper_2_list").style.width = ((numOfImage)*photoLargeWidth*pictureRatio + MarginToShift);
  document.getElementById("small_wrapper_2_list").style.marginLeft = MarginToShiftRight;
  document.getElementById("small_wrapper_2_list").style.marginRight = MarginToShift;

  //link 3 frames together
  var listA = $("#leftWrapper");
  var listB = $(".big_wrapper");
  var listC = $("#rightWrapper");
  scrollTogetherBAC.scroll(function() {
    listA.scrollLeft(scrollTogetherBAC.scrollLeft()*pictureRatio);
    listB.scrollLeft((scrollTogetherBAC.scrollLeft()));
    listC.scrollLeft(scrollTogetherBAC.scrollLeft()*pictureRatio);
  });
  scrollTogetherABC.scroll(function() {
    listA.scrollLeft(scrollTogetherABC.scrollLeft());
    listB.scrollLeft((scrollTogetherABC.scrollLeft()*(1/pictureRatio)));
    listC.scrollLeft(scrollTogetherABC.scrollLeft());
  });
  scrollTogetherCAB.scroll(function() {
    listA.scrollLeft(scrollTogetherCAB.scrollLeft());
    listB.scrollLeft((scrollTogetherCAB.scrollLeft()*(1/pictureRatio)));
    listC.scrollLeft(scrollTogetherCAB.scrollLeft());
  });

  // VIVIAN
  $('.container').css({'height':photoLargeWidth});
  $('.big_wrapper').css({width:photoLargeWidth,height:photoLargeWidth});
  $('.big_wrapper').find('.picture_container').css({height:photoLargeWidth});
  document.getElementById("big_wrapper_list").style.width = ((numOfImage)*photoLargeWidth);
  // VIVIAN
};
};
